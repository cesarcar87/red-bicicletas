let mongoose = require('mongoose');
let Bicicleta = require('../../models/bicicleta');
let Usuario = require('../../models/usuario');
let Reserva = require('../../models/reserva');

describe('Testing Usuarios', function() {
    beforeEach(function(done) {
        mongoose.disconnect();

        let mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('We are connected to test database!');
            done();
        });
    });

    afterEach(function(done) {
        Reserva.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            Usuario.deleteMany({}, function(err, success){
                if (err) console.log(err);
                Bicicleta.deleteMany(function(err, success){
                    if (err) console.log(err);
                    done();
                });
            });
        });
    });

    afterAll((done) => { mongoose.connection.close(done) });

    describe('Cuando un Usuario reserva una bici', () => {
        it('debe existir la reserva', (done) => {
            const usuario = new Usuario({nombre: 'Cesar'});
            usuario.save();
            
            const bicicleta = new Bicicleta({code: 1, color: "verde", modelo: "urbana"});
            bicicleta.save();

            let hoy = new Date();
            let maniana = new Date();
            maniana.setDate(hoy.getDate()+1);
            usuario.reservar(bicicleta.id, hoy, maniana, function(err, reserva){
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reservas) {
                    console.log(reservas[0]);
                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasDeReserva()).toBe(2);
                    expect(reservas[0].bicicleta.code).toBe(1);
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                    done();
                });
            });
        });
    });
});

/*
beforeEach(() => { Bicicleta.allBicis = [] });

describe('Bicicleta.allBicis', () => {
    it('comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe('Bicicleta.add', () => {
    it('agregamos una', () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        let a = new Bicicleta(1, 'rojo', 'urbana', [-33.243840, -58.029585]);
        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    });
});

describe('Bicicleta.findById', () => {
    it('debe devolver la bici con id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        let aBici = new Bicicleta(1, 'verde', 'urbana', [-33.243459, -58.028614]);
        let aBici2 = new Bicicleta(2, 'azul', 'montaña', [-33.243713, -58.026266]);
        Bicicleta.add(aBici);
        Bicicleta.add(aBici2);

        let targetBici = Bicicleta.findById(1);

        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(aBici.color);
        expect(targetBici.modelo).toBe(aBici.modelo);
    });
});
*/

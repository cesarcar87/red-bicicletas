let mongoose = require('mongoose');
let Bicicleta = require('../../models/bicicleta');


describe('Testing Bicicletas', function() {
    beforeEach(function(done) {
        mongoose.disconnect();

        let mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('We are connected to test database!');
            done();
        });
    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            done();
        });
    });

    afterAll((done) => { mongoose.connection.close(done) });

    describe('Bicicleta.createInstance', () => {
        it('crea una instancia de Bicicleta', () => {
            let bici = Bicicleta.createInstance(1, "verde", "urbana", [-33.243840, -58.029585]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toEqual(-33.243840);
            expect(bici.ubicacion[1]).toEqual(-58.029585);

        });
    });

    describe('Bicicleta.allBicis', () => {
        it('comienza vacia', (done) => {
            Bicicleta.allBicis(function(err, bicis) {
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add', () => {
        it('agrega solo una bici', (done) => {
            let aBici = new Bicicleta({code: 1, color: "verde", modelo: "urbana", ubicacion: [-33.243840, -58.029585]});
            Bicicleta.add(aBici, function(err, newBici) {
                if (err) console.log(err);
                Bicicleta.allBicis(function(err, bicis) {
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                    expect(bicis[0].color).toEqual(aBici.color);
                    expect(bicis[0].ubicacion[0]).toEqual(aBici.ubicacion[0]);
                    expect(bicis[0].ubicacion[1]).toEqual(aBici.ubicacion[1]);
                    done();
                });
            });
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('debe devolver la bici con code 1', (done) => {
            Bicicleta.allBicis(function(err, bicis) {
                expect(bicis.length).toBe(0);

                let aBici = new Bicicleta({code: 1, color: "verde", modelo: "urbana", ubicacion: [-33.243840, -58.029585]});
                Bicicleta.add(aBici, function(err, newBici) {
                    if (err) console.log(err);

                    let aBici2 = new Bicicleta({code: 2, color: "rojo", modelo: "urbana",ubicacion: [-33.245325, -58.035261]});
                    Bicicleta.add(aBici2, function(err, newBici) {
                        if (err) console.log(err);
                        Bicicleta.findByCode(1, function(error, targetBici) {
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toEqual(aBici.color);
                            expect(targetBici.modelo).toEqual(aBici.modelo);
                            expect(targetBici.ubicacion[0]).toEqual(aBici.ubicacion[0]);
                            expect(targetBici.ubicacion[1]).toEqual(aBici.ubicacion[1]);
                            done();
                        });
                    });
                });
            });
        });
    });

    describe('Bicicleta.removeByCode', () => {
        it('debe borrar la bici con code 1', (done) => {
            Bicicleta.allBicis(function(err, bicis) {
                expect(bicis.length).toBe(0);

                let aBici = new Bicicleta({code: 1, color: "verde", modelo: "urbana", unicacion: [-33.243840, -58.029585]});
                Bicicleta.add(aBici, function(err, newBici) {
                    if (err) console.log(err);

                    Bicicleta.allBicis(function(err, bicis) {
                        expect(bicis.length).toBe(1);

                        Bicicleta.removeByCode(1, function(error, response) {
                            Bicicleta.allBicis(function(err, bicis) {
                                expect(bicis.length).toBe(0);
                                done();
                            });
                        });
                    });
                });
            });
        });
    });

});

/*
beforeEach(() => { Bicicleta.allBicis = [] });

describe('Bicicleta.allBicis', () => {
    it('comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe('Bicicleta.add', () => {
    it('agregamos una', () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        let a = new Bicicleta(1, 'rojo', 'urbana', [-33.243840, -58.029585]);
        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    });
});

describe('Bicicleta.findById', () => {
    it('debe devolver la bici con id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        let aBici = new Bicicleta(1, 'verde', 'urbana', [-33.243459, -58.028614]);
        let aBici2 = new Bicicleta(2, 'azul', 'montaña', [-33.243713, -58.026266]);
        Bicicleta.add(aBici);
        Bicicleta.add(aBici2);

        let targetBici = Bicicleta.findById(1);

        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(aBici.color);
        expect(targetBici.modelo).toBe(aBici.modelo);
    });
});
*/

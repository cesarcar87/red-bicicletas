"use sctrict";

let express = require('express'),
    router = express.Router(),
    usuarioControllerApi = require('../../controllers/api/usuarioControllerAPI');

router.get('/', usuarioControllerApi.usuarios_list);
router.post('/create', usuarioControllerApi.usuarios_create);
router.post('/reservar', usuarioControllerApi.usuario_reservar);

module.exports = router;

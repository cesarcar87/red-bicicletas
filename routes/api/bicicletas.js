"use sctrict";

let express = require('express'),
    router = express.Router(),
    bicicletaControllerApi = require('../../controllers/api/bicicletaControllerAPI');

router.get('/', bicicletaControllerApi.bicicleta_list);
router.post('/create', bicicletaControllerApi.bicicleta_create);
router.post('/:id/update', bicicletaControllerApi.bicicleta_update);
router.post('/:id/delete', bicicletaControllerApi.bicicleta_delete);

module.exports = router;

"use sctrict";

const passport = require('passport');

const express = require('express'),
    router = express.Router(),
    authControllerApi = require('../../controllers/api/authControllerAPI');

router.post('/authenticate', authControllerApi.authenticate);
router.post('/forgotPassword', authControllerApi.forgotPassword);
router.post('/facebook_token', passport.authenticate('facebook-token'), authControllerApi.authFacebookToken);

module.exports = router;

"use strict";

let express = require('express'),
    router = express.Router(),
    tokenController = require('../controllers/token');

router.get('/confirmation/:token', tokenController.confirmationGet);

module.exports = router;
